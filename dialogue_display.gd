@tool
class_name DialogueContainer extends HBoxContainer
enum audio{normal=0,emot=1,letters=2}
enum mode{normal=0,bubble=1,film=2}
@onready var audio_player:=AudioStreamPlayer.new()
@export var current_audio:audio=audio.letters
@export var dialogue_mode:mode:
	set(val):
		dialogue_mode=val
		if dialogue_mode==mode.normal:
			anchor_top=0.7
			anchor_bottom=0.9
			anchor_left=0.099
			anchor_right=0.96
		else:
			anchor_top=0
			anchor_bottom=1
			anchor_left=0
			anchor_right=1
var group:ButtonGroup=ButtonGroup.new()
@export var fade_speed:=0.25
@export var instant_dialogue:bool=false
@export var dialogue_speed :float= 45
@export var text="":
	set(val):
		text=val
		if nodes.has("RichTextLabel"):
			nodes["RichTextLabel"].text=text
var start_arow_pos:Vector2
var texture=load("res://addons/DialogueServer/objects/DownArrow.svg")
var target
var nodes:={}
signal choice
signal play_track
func _init(_target,instant:=false,_audio:audio=audio.emot,new_mode:mode=mode.normal):
	self.visible=false
	self.dialogue_mode=new_mode
	self.instant_dialogue=instant
	self.target=_target
	self.current_audio=_audio
func _ready():
	modulate.a=0
	grow_horizontal=Control.GROW_DIRECTION_BOTH
	grow_vertical=Control.GROW_DIRECTION_BOTH
	set("theme_override_constants/separation",70)
	audio_player.bus="VO"
	add_child(audio_player)
	nodes["panel"]=PanelContainer.new()
	add_child(nodes["panel"])
	nodes["panel"].size_flags_horizontal=3
	nodes["panel"].size_flags_vertical=1
	nodes["HBoxContainer"]=HBoxContainer.new()
	nodes["panel"].add_child(nodes["HBoxContainer"])
	nodes["VBoxContainer"]=VBoxContainer.new()
	nodes["HBoxContainer"].add_child(nodes["VBoxContainer"])
	nodes["VBoxContainer"].size_flags_horizontal=3
	nodes["VBoxContainer"].size_flags_vertical=1
	nodes["RichTextLabel"]=RichTextLabel.new()
	nodes["VBoxContainer"].add_child(nodes["RichTextLabel"])
	nodes["RichTextLabel"].fit_content_height=true
	nodes["RichTextLabel"].scroll_active=false
	nodes["RichTextLabel"].autowrap_mode=3
	nodes["RichTextLabel"].visible_characters_behavior=2
	nodes["RichTextLabel2"]=RichTextLabel.new()
	nodes["VBoxContainer"].add_child(nodes["RichTextLabel2"])
	nodes["RichTextLabel2"].visible_characters=0
	nodes["RichTextLabel2"].fit_content_height=true
	nodes["RichTextLabel2"].scroll_active=false
	nodes["RichTextLabel2"].autowrap_mode=3
	nodes["RichTextLabel2"].visible_characters_behavior=2
	nodes["RichTextLabel2"].modulate.a=0
	nodes["VBoxContainer2"]=VBoxContainer.new()
	nodes["HBoxContainer"].add_child(nodes["VBoxContainer2"])
	nodes["VBoxContainer2"].size_flags_horizontal=3
	nodes["VBoxContainer2"].size_flags_vertical=1
	nodes["VBoxContainer2"].size_flags_stretch_ratio=0.04
	nodes["MarginContainer"]= MarginContainer.new()
	nodes["VBoxContainer2"].add_child(nodes["MarginContainer"])
	nodes["MarginContainer"].size_flags_horizontal=1
	nodes["MarginContainer"].size_flags_vertical=3
	nodes["TextureRect"]= TextureRect.new()
	nodes["VBoxContainer2"].add_child(nodes["TextureRect"])
	nodes["TextureRect"].stretch_mode=4
	nodes["TextureRect"].texture=texture
	nodes["TextureRect"].size_flags_horizontal=1
	nodes["TextureRect"].size_flags_vertical=3
	nodes["TextureRect"].size_flags_stretch_ratio=0.16
	nodes["TextureRect"].modulate.a=0
	start_arow_pos=nodes["TextureRect"].position
	nodes["ScrollContainer"]=ScrollContainer.new()
	add_child(nodes["ScrollContainer"])
	nodes["ScrollContainer"].follow_focus=true
	nodes["ScrollContainer"].size_flags_stretch_ratio=0.3
	nodes["ScrollContainer"].size_flags_horizontal=3
	nodes["ScrollContainer"].size_flags_vertical=1
	nodes["VBoxContainer3"]=VBoxContainer.new()
	nodes["ScrollContainer"].add_child(nodes["VBoxContainer3"])
	nodes["VBoxContainer3"].size_flags_horizontal=3
func fade_in():
	show()
	self.modulate.a=1
	var fade:=create_tween()
	fade.tween_property(self,"modulate",Color(1,1,1,1),fade_speed)
	await fade.finished
func fade_out():
	self.modulate.a=0
	var fade:=create_tween()
	fade.tween_property(self,"modulate",Color(1,1,1,0),fade_speed)
	await fade.finished
	hide()
func down_arrow_animation(active:=true):
	var tween_down_arrow:=create_tween()
	if active:
		tween_down_arrow.tween_property(nodes["TextureRect"],"modulate",Color(1,1,1,1),0.1)
	else:
		tween_down_arrow.tween_property(nodes["TextureRect"],"modulate",Color(1,1,1,0),0.1)
	await tween_down_arrow.finished
	if !active:
		tween_down_arrow.kill()
		nodes["TextureRect"].position.y=start_arow_pos.y
		return
	tween_down_arrow=create_tween().set_loops().set_trans(Tween.TRANS_CUBIC)
	tween_down_arrow.tween_property(nodes["TextureRect"],"position:y",nodes["TextureRect"].position.y+3.5,1).set_trans(Tween.TRANS_CUBIC)
	tween_down_arrow.tween_interval(0.1)
	tween_down_arrow.tween_property(nodes["TextureRect"],"position:y",nodes["TextureRect"].position.y-3.5,1).set_trans(Tween.TRANS_CUBIC)
func set_title(title:=""):
	nodes["RichTextLabel"].parse_bbcode(title)
	nodes["RichTextLabel"].text=nodes["RichTextLabel"].get_parsed_text()

func set_text(text:=""):
	if instant_dialogue:
		nodes["RichTextLabel2"].percent_visible=0.0
		nodes["RichTextLabel2"].parse_bbcode(text)
		nodes["RichTextLabel2"].text=nodes["RichTextLabel2"].get_parsed_text()
		nodes["RichTextLabel2"].percent_visible=1.0
		nodes["RichTextLabel2"].modulate.a=1
	else:
		var dialog_tween:Tween=create_tween()
		dialog_tween.tween_property(nodes["RichTextLabel2"],"modulate",Color(1,1,1,0),0.25)
		await dialog_tween.finished
		nodes["RichTextLabel2"].percent_visible=0.0
		nodes["RichTextLabel2"].parse_bbcode(text)
		nodes["RichTextLabel2"].percent_visible=0.0
		var text_holder:String=nodes["RichTextLabel2"].get_parsed_text()
		var time:float=dialogue_speed/float(text_holder.length())
		dialog_tween=create_tween()
		dialog_tween.tween_property(nodes["RichTextLabel2"],"modulate",Color(1,1,1,1),0.25)
		await dialog_tween.finished
		while nodes["RichTextLabel2"].visible_characters<text_holder.length():
			await get_tree().create_timer(0.05).timeout
			nodes["RichTextLabel2"].visible_characters+=1
			if current_audio==audio.letters:
				play_track.emit()
func can_skip()->bool:
	if nodes["RichTextLabel2"].percent_visible<1.0:
		return true
	else:
		return false
func skip():
	nodes["RichTextLabel2"].visible_characters=999999
func pick_a_choice():
	emit_signal("choice",str(group.get_pressed_button().name))

func create_choices(choices:=[])->void:
	down_arrow_animation(false)
	if nodes["VBoxContainer3"].get_child_count()!=0:
		await remove_choices()
	if choices.is_empty():
		return
	for i in choices:
		var button=Button.new()
		button.size_flags_horizontal=3
		button.set_script(UI)
		nodes["VBoxContainer3"].add_child(Control.new())
		nodes["VBoxContainer3"].add_child(button)
		button.set_button_group(group)
		button.connect("pressed",pick_a_choice)
		if i.has("choice_key"):
			button.text=str(i["choice_key"][TranslationServer.get_locale()])
		else:
			button.text=str(i["index"])
		button.name=StringName(str(i["index"]))
	nodes["VBoxContainer3"].add_child(Control.new())
	var buttons_tween:=create_tween()
	buttons_tween.tween_property(nodes["VBoxContainer3"],"modulate",Color(1,1,1,1),0.25)
	await buttons_tween.finished
	nodes["VBoxContainer3"].get_children()[1].grab_focus()

func remove_choices():
	var remove_buttons_tween:=create_tween()
	remove_buttons_tween.tween_property(nodes["VBoxContainer3"],"modulate",Color(1,1,1,0),0.25)
	await remove_buttons_tween.finished
	for i in nodes["VBoxContainer3"].get_children():
		i.queue_free()

