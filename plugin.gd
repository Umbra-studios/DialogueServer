@tool
extends EditorPlugin

func _enter_tree():
	add_autoload_singleton("DialogueServer", "res://addons/DialogueServer/DialogueServer.gd")


func _exit_tree():
	remove_autoload_singleton("DialogueServer")
